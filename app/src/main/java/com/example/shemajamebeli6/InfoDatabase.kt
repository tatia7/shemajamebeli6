package com.example.shemajamebeli6

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Info::class], version = 1, exportSchema = false)
abstract class InfoDatabase : RoomDatabase() {

    abstract fun ItemDao(): ItemDao

    companion object {
        @Volatile
        private var INSTANCE : InfoDatabase? = null

        fun getDatabase(context: Context): InfoDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }else{
                synchronized(this){
                    val instace = Room.databaseBuilder(
                        context.applicationContext,
                        InfoDatabase::class.java,
                        "info_database"
                    ).build()
                    INSTANCE = instace
                    return instace
                }
            }
        }
    }
}