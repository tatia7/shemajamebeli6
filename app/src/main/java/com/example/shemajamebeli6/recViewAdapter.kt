package com.example.shemajamebeli6

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_add_info.view.*
import kotlinx.android.synthetic.main.infoview.view.*

class recViewAdapter() : RecyclerView.Adapter<recViewAdapter.infoViewHolder>() {

    private var info = emptyList<Info>()
    class infoViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): infoViewHolder {
        return infoViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.infoview,parent,false))
    }


    override fun getItemCount() = info.size

    override fun onBindViewHolder(holder: infoViewHolder, position: Int) {
        val information = info[position]
        holder.itemView.titleTextView.text = information.title
        holder.itemView.decsription.text = information.description
    }
    fun setData(info : List<Info>){
        this.info = info
        notifyDataSetChanged()
    }

}