package com.example.shemajamebeli6

import android.content.ClipData
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_add_info.*
import kotlinx.android.synthetic.main.fragment_add_info.view.*

class FragmentAddInfo : Fragment() {

    private lateinit var itemViewModel : ItemViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_info, container, false)


        itemViewModel =  ViewModelProvider(this).get(ItemViewModel::class.java)

        view.addBtn.setOnClickListener {
            addToDatabase()
        }
        return view
    }
    private fun addToDatabase(){
        val title = edTitle.text.toString()
        val desc = edDesc.text.toString()
        if (checkText(title, desc)){
            val info = Info(0, title,desc, "https://techcrunch.com/wp-content/uploads/2017/02/android-studio-logo.png?w=730&crop=1")

            itemViewModel.addInfo(info)
            Toast.makeText(requireContext(), "Added!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_fragmentAddInfo_to_fragmentRecView)
        }else{
            Toast.makeText(requireContext(), "Fill!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkText(title : String, desc : String): Boolean{
        if (title.isNotEmpty() && desc.isNotEmpty()){
            if (title.length in 5..30 && desc.length in 32..300){
                return true
            }
        }else{
            return false
        }
        return false
    }

}