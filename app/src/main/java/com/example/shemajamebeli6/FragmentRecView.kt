package com.example.shemajamebeli6

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_rec_view.view.*
import android.widget.ListAdapter as ListAdapter

class FragmentRecView : Fragment() {

    private lateinit var itemViewModel: ItemViewModel
    private lateinit var adapter: recViewAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_rec_view, container, false)
        adapter = recViewAdapter()
        val recView = view?.recView
        recView?.adapter = adapter
        recView?.layoutManager = LinearLayoutManager(requireContext())

        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)
        itemViewModel.readAllData.observe(viewLifecycleOwner, Observer{ info ->
            adapter.setData(info)

        })
        view.btn_add.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentRecView_to_fragmentAddInfo)
        }
        return view
    }
    private fun init(){
    }

}