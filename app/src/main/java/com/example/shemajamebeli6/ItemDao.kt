package com.example.shemajamebeli6

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface ItemDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addItem(info: Info)
    @Query("SELECT* FROM item_table ORDER BY id ASC")
    fun readallData(): LiveData<List<Info>>
}