package com.example.shemajamebeli6

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ItemViewModel(application: Application) : AndroidViewModel(application) {

    val readAllData : LiveData<List<Info>>
    private val repository : InfoRepository

    init {
        val itemDao = InfoDatabase.getDatabase(application).ItemDao()
        repository = InfoRepository(itemDao)
        readAllData = repository.readAllData
    }

    fun addInfo(info: Info){

        viewModelScope.launch(Dispatchers.IO){
            repository.addInfo(info)
        }
    }
}