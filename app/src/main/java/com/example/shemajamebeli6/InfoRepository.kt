package com.example.shemajamebeli6

import androidx.lifecycle.LiveData

class InfoRepository(private val itemDao: ItemDao) {

    val readAllData: LiveData<List<Info>> = itemDao.readallData()

    suspend fun addInfo(info: Info){
        itemDao.addItem(info)
    }
}